package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {
	private int community_area;
	private String tripId;
	private  String taxiId;
	private  int tripSeconds;
	private double tripMiles;
	private  double tripTotal;
	
	public Service(int cm, String tId, String taId, int tS, double tM, double tT)
	{
		tripId=tId;
		community_area=cm;
		taxiId=taId;
		tripSeconds= tS;
		tripMiles= tM;
		tripTotal=tT;
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return "trip Id";
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return "taxi Id";
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getCommunity_area() {
		return community_area;
	}

	public void setCommunity_area(int community_area) {
		this.community_area = community_area;
	}
}
