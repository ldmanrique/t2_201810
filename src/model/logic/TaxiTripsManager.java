package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;
import model.data_structures.MyLinkedList;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	LinkedList services;
	LinkedList taxis;
	ArrayList<String> ids;




	public void loadServices (String serviceFile) {
		// TODO Auto-generated method stub

		LinkedList<Service> services= new <Service> MyLinkedList();
		LinkedList <Taxi> taxis= new MyLinkedList<Taxi>();
		ArrayList<String> ids= new ArrayList<>();
		JSONParser parser =new JSONParser();
		try
		{
			JSONArray listaGson= (JSONArray)parser.parse(new FileReader(serviceFile));
			Iterator iter=  listaGson.iterator();
			int x= 0;

			while(iter.hasNext())
			{
				JSONObject o=(JSONObject) listaGson.get(x);

				String company= "Independents";
				if(o.get("Company") != null) {
					company= (String) o.get("Company");
				}
				int community =0;
				if(o.get("dropoff_community_area") != null) {
					community= Integer.parseInt((String) o.get("dropoff_community_area"));
				}

				String tripId= (String) o.get("trip_id");
				String taxiId= (String) o.get("taxi_id");
				int tripSeconds= Integer.parseInt((String) o.get("trip_seconds"));
				double tripMiles= Double.parseDouble((String) o.get("trip_miles"));
				double tripTotal= Double.parseDouble((String) o.get("trip_total"));

				Service serviciox = new Service(community, tripId, taxiId, tripSeconds, tripMiles, tripTotal);
				services.add(serviciox);

				Taxi taxix= new Taxi(company, taxiId);
				if(!ids.contains(taxiId))
				{

				}
				taxis.add(taxix);

				iter.next();
				x++;
			}

			System.out.println("La cantidad de servicios cargados fue: " + services.size());
			System.out.println("Se cargaron "+ taxis.size()+ " taxis");



		}

		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		// TODO Auto-generated method stub
		LinkedList <Taxi> listaTaxis= new MyLinkedList<>();

		for(int i=0; i<taxis.size(); i++)
		{
			Taxi x= (Taxi) taxis.getI(i);
			if(x.getCompany().equals(company))
			{
				listaTaxis.add(x);
			}
		}



		System.out.println("Inside getTaxisOfCompany with " + company);
		return listaTaxis;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		// TODO Auto-generated method stub
		LinkedList <Service> listaServicios= new MyLinkedList<>();

		for(int i=0; i<services.size(); i++)
		{
			Service x= (Service) services.getI(i);
			if(x.getCommunity_area()==communityArea)
			{
				listaServicios.add(x);
			}
		}

		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		return listaServicios;
	}


}
