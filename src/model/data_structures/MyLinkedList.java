package model.data_structures;

public class MyLinkedList<T extends Comparable<T>> implements LinkedList<T>
{

	//First node
	private Node<T> head;

	//Actual Node
	private Node<T> actual;

	//Size of the list
	private int size;



	public MyLinkedList() {
		// TODO Auto-generated constructor stub
		head = null;
		actual = null;

		size = 0;
	}

	@Override
	public void add(T element) {
		// TODO Auto-generated method stub
		if (this.get(element) != null ) return; 
		System.out.println("head es" + head);
		if(head == null) {
			head = new Node<T>(element);
			actual = head  ;
			size = 1;
		}
		else 
		{
			Node<T> actual = head;
			while( actual.hasNext()) {
				actual = actual.getNext();
			}
			actual.setNextNode(new Node<T>(element));
			actual.getNext().setPrevious(actual);
			size++;

		}

	}

	@Override
	public boolean remove(T element) {
		// TODO Auto-generated method stub
		if (this.head == null) return false;
		else if (this.head.getItem().equals(element)) {
			this.head = this.head.getNext();
			head.setPrevious(null);
			size--;
			return true;
		}
		else {
			Node<T> actual = head;
			while(actual.hasNext()) {
				Node<T> next = actual.getNext();
				if (next.getItem().compareTo(element) == 0) {
					next.getNext().setPrevious(actual);
					actual.setNextNode(next.getNext());

					size--;
					return true;
				}
				actual = actual.getNext();
			}
		}
		return false;
	}

	@Override
	public T get(T element) {
		// TODO Auto-generated method stub
		if(head != null) {
			Node<T> act = head;
			if(act.getItem().compareTo(element) == 0) return element;
			while(act.hasNext()) {
				act = act.getNext();
				if (act.getItem().compareTo(element) == 0) return element;				
			}	
		}

		return null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public T getI(int index) throws IndexOutOfBoundsException{
		if(index >= size) {
			throw new IndexOutOfBoundsException("Size: "+ size + ". Index: " + index);
		}
		else {
			Node<T> actual = head;
			while(index > 0) {
				actual = actual.getNext();
				index = index-1;
			}
			return actual.getItem();
		}
	}

	@Override
	public T[] listing(T[] array) {
		// TODO Auto-generated method stub
		Node<T> actual = head;
		int count = 0;
		if (actual != null) {
			array[count] = actual.getItem();
			count++;
		}
		while(actual.hasNext()) {
			actual = actual.getNext();
			array[count] = actual.getItem();
			count++;
		}
		return array;
	}

	@Override
	public T getCurrent() {
		// TODO Auto-generated method stub
		return actual.getItem();
	}

	@Override
	public T next() {
		// TODO Auto-generated method stub
		actual = actual.getNext();
		return actual==null?null:actual.getItem();
	}

	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		if(actual == null) {
			return false;
		}
		else {
			return actual.getNext() == null;
		}
	}

	@Override
	public T first() {
		// TODO Auto-generated method stub
		actual = head;
		return actual.getItem();
	}

	@Override
	public void set(T element, int index) throws IndexOutOfBoundsException{
		// TODO Auto-generated method stub
		if(index >= size) {
			throw new IndexOutOfBoundsException("Size: "+ size + ". Index: " + index);
		}
		else {
			Node<T> act = head;
			while(index > 0) {
				act = act.getNext();
				index = index-1;
			}

			act.setItem(element);
		}
	}



	@Override
	public void delete(int index) {
		// TODO Auto-generated method stub
		if(index >= size) {
			throw new IndexOutOfBoundsException("Size: "+ size + ". Index: " + index);
		}
		else if(index == 0) {
			head = head.getNext();
			head.setPrevious(null);
			size--;
		}
		else {
			Node<T> act = head;
			while(index > 0) {
				act = act.getNext();
				index = index-1;
			}
			act.getNext().setPrevious(act.getPrevious());
			act.getPrevious().setNextNode(act.getNext());

			size--;
		}
	}

	@Override
	public T previous() {
		// TODO Auto-generated method stub
		return actual.getPrevious().getItem();
	}

	@Override
	public boolean hasPrevious() {
		// TODO Auto-generated method stub
		if(actual == null) {
			return false;
		}
		else {
			return actual.getPrevious() == null;
		}
	}
}
