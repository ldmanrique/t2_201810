package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add: add a new element T 
 * delete: delete the given element T 
 * get: get the given element T (null if it doesn't exist in the list)
 * size: return the the number of elements
 * get: get an element T by position (the first position has the value 0) 
 * listing: set the listing of elements at the first element
 * getCurrent: return the current element T in the listing (return null if it doesn�t exists)
 * next: advance to next element in the listing (return if it exists)
 * @param <T>
 */
public interface LinkedList <T extends Comparable<T>>  {

	public abstract void add(T element);

	public abstract boolean remove(T element);

	public abstract	T get(T element);

	public abstract	 int size();



	public abstract	 T[] listing(T[] array);

	public abstract	 T getCurrent();

	public abstract	 T next();

	public abstract	 boolean hasNext();

	public abstract		T first();

	public abstract	 void set(T element, int index) throws IndexOutOfBoundsException;

	public abstract	 T previous();

	public abstract	 boolean hasPrevious();

	void delete(int index);

	T getI(int index) throws IndexOutOfBoundsException;

	

	



}
