package Tests;


import static org.junit.Assert.*;
import model.data_structures.MyLinkedList;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;





public class TestMyLinkedList {




	private MyLinkedList lista;


	@Before
	public void setupEscenario1( )
	{

		lista = new MyLinkedList<>();
		lista.add(5);
		lista.add(8);
		lista.add(10);

	}



	// -----------------------------------------------------------------
	// M�todos de prueba
	// -----------------------------------------------------------------

	/**
	 * add
	 * remove
	 * get
	 * size
	 * get by position
	 * listing
	 * getCurrent
	 * next
	 */
	@Test
	public void testAdd()
	{
		lista.add(6);
		assertEquals( "El tama�o de la lista no es correcto", lista.size(), 4 );

	}

	@Test
	public void testRemove()
	{


		assertTrue("No est� eliminando" , lista.remove(5));
	}

	@Test
	public void testGet()
	{
		
		assertEquals( "El elemento no es el deseado", lista.get(5), 5 );

	}
	
	@Test
	public void testSize()
	{
		lista.add(9);	
		assertEquals( "El elemento no es el deseado", lista.size(), 4 );

	}
	
	@Test
	public void TestGetI()
	{
	
		assertEquals( "El elemento no es el deseado", lista.getI(0), 5 );

	}
	
	@Test
	public void TestgetCurrent()
	{
	
		assertEquals( "El elemento no es el deseado", lista.getI(0), 5 );

	}

	
}
